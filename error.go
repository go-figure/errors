package errors

import (
	"fmt"
	"strings"
)

type Error struct {
	Code    *Code
	Message string
	Where   []Frame

	Cause   error
	Masking bool
}

func New(message string) *Error {
	return &Error{
		Code:    Generic,
		Message: strings.TrimSpace(message),
		Where:   location(1, false),
	}
}

func Newf(format string, args ...interface{}) *Error {
	return &Error{
		Code:    Generic,
		Message: strings.TrimSpace(fmt.Sprintf(format, args...)),
		Where:   location(1, false),
	}
}

func Trace(err error) *Error {
	return &Error{
		Code:  Generic,
		Where: location(1, false),
		Cause: err,
	}
}

func (e *Error) WithCode(code *Code) *Error {
	e.Code = code
	return e
}

func (e *Error) Wraps(cause error) *Error {
	e.Cause = cause
	e.Masking = false
	return e
}

func (e *Error) Masks(cause error) *Error {
	e.Cause = cause
	e.Masking = true
	return e
}

func (e *Error) Error() string {
	if e.Cause == nil || e.Masking {
		return e.Message
	}

	if len(e.Message) == 0 {
		return e.Cause.Error()
	}

	return fmt.Sprintf("%s: %s", e.Message, e.Cause.Error())
}

func Is(code *Code, err error) bool {
	cerr, ok := err.(*Error)
	if !ok {
		return false
	}

	return cerr.Code == code
}

func Cause(err error) error {
	cerr, ok := err.(*Error)
	if !ok {
		return nil
	}

	return cerr.Cause
}
