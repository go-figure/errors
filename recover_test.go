package errors_test

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/go-figure/errors"
)

func TestRecover(t *testing.T) {
	tests := []struct {
		name string
		cerr error
		perr error
	}{
		{
			name: "3",
			cerr: errors.New(errors.Generic, "wat"),
			perr: errors.New(errors.Generic, "waka"),
		},
	}

	spew.Config.ContinueOnMethod = true

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			err := panickingFunc(test.cerr, test.perr)
			spew.Dump(err)
		})
	}
}

func panickingFunc(cerr, perr error) (err error) {
	defer errors.Recover(&err)

	err = cerr
	if perr != nil {
		panic(perr)
	}

	return
}
